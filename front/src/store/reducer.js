import {ADD_SUCCESS, GET_SUCCESS} from "./actionTypes";

const initialState = {
    posts: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_SUCCESS:
            return {...state, posts: action.posts};
        case ADD_SUCCESS:
            return {...state, posts: {...state.posts, post}};
        default:
            return state;
    }
};

export default reducer;