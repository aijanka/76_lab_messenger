import axios from 'axios';
import {GET_SUCCESS} from "./actionTypes";


export const getSuccess = (posts) => ({type: GET_SUCCESS, posts});
export const getPosts = () => {
    return (dispatch) => {
        axios.get('/messages').then(response => dispatch(getSuccess(response.data)))
    }
};


export const addSuccess = (posts) => ({type: GET_SUCCESS, posts});
export const addPost = (post) => {
    return (dispatch) => {
        axios.post('/messages', post).then(response => dispatch(addSuccess(response.data)))
    }
};