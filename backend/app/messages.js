const express = require('express');
const router = express.Router();

const createRouter = (db) => {
    router.get('/', (req, res) => {
        if(req.query.datetime) {
            res.send(db.getByDate(req.query.datetime));
        } else {
            res.send(db.getPosts());
        }
    });

    router.post('/', (req, res) => {
        const post = req.body;
        db.addPost(post).then(result => res.send(result));
    });

    return router;
};

module.exports = createRouter;