const express = require('express');
const cors = require("cors");
const app = express();
const port = 8000;
const messagesDb = require('./messagesDb');
const messages = require('./app/messages');

app.use(cors());
app.use(express.json());

messagesDb.init().then(() => {
    app.use('/messages', messages(messagesDb));
    app.listen(port);
})
