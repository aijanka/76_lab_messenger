const fs = require('fs');
const nanoid = require('nanoid');

let posts = null;

module.exports = {
    init: () => {
        return new Promise((resolve, reject) => {
            fs.readFile('./db.json', (err, result) => {
                if(err) {
                    reject(err);
                } else {
                    posts = JSON.parse(result);
                    console.log(posts, 'posts');
                    resolve();
                }
            })
        })
    },
    getPosts: () => {
        return posts.filter((post, index) => index < 30)
    },
    addPost: post => {
        post.id = nanoid();
        post.datetime = new Date().toISOString();
        posts.push(post);

        let contents = JSON.stringify(posts, null, 2);

        return new Promise ((resolve, reject) => {
            fs.writeFile('./db.json', contents, err => {
                if (err) {
                    reject(err);
                } else {
                    resolve(post);
                }
            })
        })

    },
    getByDate: (date) => {
        return posts.filter(post => post.datetime < date);
    }
}