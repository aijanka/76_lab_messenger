import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import SendMessageForm from "./components/SendMessageForm/SendMessageForm";
import MessagePanel from "./components/MessagePanel/MessagePanel";
import {addPost, getPosts} from "./store/actions";
import './App.css';

class App extends Component {
    state = {
        currentMessage: '',
        currentAuthor: '',
        posts: []
    };

    // interval = '';
    //
    // getPosts(url) {
    //     return fetch(url)
    //         .then(response => {
    //             if (response.ok) {
    //                 return response.json();
    //             }
    //             throw new Error('Something went wrong with network request');
    //         })
    //         .then(posts => {
    //             this.setState({posts});
    //         })
    // }

    scrollToBottom = () => {
        this.messagesEnd.scrollIntoView({ behavior: "smooth" });
    };

    // componentWillMount() {
    //
    //     this.getPosts('http://146.185.154.90:8000/messages');
    //
    // }

    // componentDidUpdate() {
    //     this.scrollToBottom();
    // }
    //
    // componentDidMount() {
    //     this.interval = setInterval(() => this.getPosts('http://146.185.154.90:8000/messages'), 5000);
    //     this.scrollToBottom();
    // }
    //
    // componentWillUnmount() {
    //     clearInterval(this.interval);
    // }
    //
    // shouldComponentUpdate(nextState){
    //     return nextState !== this.state;
    // }
    //
    // addMessageToServer(e) {
    //     e.preventDefault();
    //
    //
    //     // const data = new URLSearchParams();
    //     // data.append('message', this.state.currentMessage);
    //     // data.append('author', this.state.currentAuthor);
    //     //
    //     // const config = {
    //     //     method: 'POST',
    //     //     body: data
    //     // };
    //     //
    //     // fetch('http://146.185.154.90:8000/messages', config)
    //     //     .then(response => this.interval = setInterval(() => this.getPosts('http://146.185.154.90:8000/messages'), 5000));
    //
    // }

    componentDidMount() {
        this.props.getPosts();
        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }


    saveCurrentMessage(event) {
        this.setState({currentMessage: event.target.value});
    }

    saveCurrentAuthor(event) {
        this.setState({currentAuthor: event.target.value});
    }

    addPost = (event) => {
        event.preventDefault();
        const post = {
            message: this.state.currentMessage,
            author: this.state.currentAuthor
        };
        this.props.addPost(post);
        this.setState({currentMessage: '', currentAuthor: ''})
        this.forceUpdate();
    }

    render() {
        return (
            <Fragment>

                <SendMessageForm
                    currentMessage={(event) => this.saveCurrentMessage(event)}
                    currentAuthor={(event) => this.saveCurrentAuthor(event)}
                    author={this.state.currentAuthor}
                    message={this.state.currentMessage}
                    addPost={this.addPost}
                />

                <div className="MessagesWrapper">
                    {this.props.posts.map((post, index) => (
                            <MessagePanel
                                key={(Date.now() + index).toString()}
                                datetime={post.datetime}
                                message={post.message}
                                author={post.author}
                            />
                        )
                    )}
                <div style={{ float:"left", clear: "both" }} ref={(el) => { this.messagesEnd = el; }}/>
                </div>

            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    posts: state.posts
});

const mapDispatchToProps = dispatch => ({
    getPosts: () => dispatch(getPosts()),
    addPost: (post) => dispatch(addPost(post))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
