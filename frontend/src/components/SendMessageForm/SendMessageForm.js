import React from 'react';
import FormGroup from "react-bootstrap/es/FormGroup";
import Form from "react-bootstrap/es/Form";
import ControlLabel from "react-bootstrap/es/ControlLabel";
import Button from "react-bootstrap/es/Button";
import FormControl from "react-bootstrap/es/FormControl";
import './SendMessageForm.css';

const SendMessageForm = props => {
    return (
        <Form inline>

            <FormGroup controlId="formInlineName">
                <ControlLabel>Name</ControlLabel>{' '}
                <FormControl
                    type="text"
                    placeholder="Your name"
                    onChange={props.currentAuthor}
                    value={props.author}
                />
            </FormGroup>{' '}

            <FormGroup controlId="formInlineEmail">
                <ControlLabel>Message</ControlLabel>{' '}
                <FormControl
                    type="text"
                    placeholder="Your message"
                    onChange={props.currentMessage}
                    value={props.message}
                />
            </FormGroup>{' '}

            <Button
                type="submit"
                onClick={props.addPost}
                bsStyle="success"
            >
                Submit
            </Button>
        </Form>
    )
};

export default SendMessageForm;