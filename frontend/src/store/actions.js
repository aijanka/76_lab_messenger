import axios from 'axios';
import {ADD_SUCCESS, GET_SUCCESS} from "./actionTypes";


export const getSuccess = (posts) => ({type: GET_SUCCESS, posts});
export const getPosts = () => {
    return (dispatch) => {
        console.log('i am in getPosts');
        axios.get('/messages').then(response => dispatch(getSuccess(response.data)))
    }
};


export const addSuccess = (post) => ({type: ADD_SUCCESS, post});
export const addPost = (post) => {
    return (dispatch) => {
        console.log(post);
        axios.post('/messages', post).then(response => {
            console.log(response.data, 'sent to server post')
            dispatch(addSuccess(response.data))
        })
    }
};